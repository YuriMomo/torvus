use std::{
    env,
    net::SocketAddr,
    sync::{Arc, Mutex, mpsc},
    time::Duration,
    str::FromStr,
    thread,
};
use serenity::{
    prelude::*,
    model::{
        gateway::{Ready, Game, GameType},
        event::ResumedEvent,
        channel::Message,
        id::{ChannelId, GuildId},
        user::OnlineStatus,
    },
    utils::Colour,
};
use veloren_client::{
    Client as VelorenClient,
    Event as VelorenEvent,
    Input as VelorenInput,
};
use veloren_common::{
    comp,
    clock::Clock,
};

const TPS: u64 = 60;

enum VelorenMessage {
    Connect,
    Disconnect,
    Chat(String),
}

enum DiscordMessage {
    Chat {
        nickname: String,
        msg: Message,
    },
}

struct Handler {
    messages_rx: Mutex<Option<mpsc::Receiver<VelorenMessage>>>,
    server_addr: SocketAddr,
    guild: u64,
    bridge_channel: u64,
    discord_msgs_tx: Mutex<mpsc::Sender<DiscordMessage>>,
}

impl Handler {
    pub fn new<A: Into<SocketAddr> + Clone>(addr: A, guild: u64, bridge_channel: u64) -> Self {
        let veloren_client = Arc::new(Mutex::new(VelorenClient::new(addr.clone().into(), comp::Player::new("D".to_string()), None, 0)
            .expect("Failed to create Veloren client instance")));

        let (messages_tx, messages_rx) = mpsc::channel();
        let (discord_msgs_tx, discord_msgs_rx) = mpsc::channel();

        let socket = addr.clone().into();
        let veloren_client_ref = veloren_client.clone();
        let messages_ref = Mutex::new(messages_tx);
        thread::spawn(move || {
            let mut clock = Clock::new();
            let messages_tx = messages_ref.into_inner().unwrap();

            'connect: loop {
                let mut veloren_client = match VelorenClient::new(socket, comp::Player::new("Discord".to_string()), None, 0) {
                    Ok(client) => client,
                    Err(_) => {
                        thread::sleep(Duration::from_secs(5));
                        continue 'connect;
                    },
                };

                messages_tx.send(VelorenMessage::Connect).unwrap();

                loop {
                    let events = match veloren_client.tick(VelorenInput::default(), clock.get_last_delta()) {
                        Ok(events) => events,
                        Err(_) => {
                            thread::sleep(Duration::from_secs(5));
                            messages_tx.send(VelorenMessage::Disconnect).unwrap();
                            continue 'connect;
                        },
                    };

                    for DiscordMessage::Chat { nickname, msg } in discord_msgs_rx.try_recv() {
                        let msg: Message = msg;
                        veloren_client.send_chat(format!("/alias D:{}", nickname));
                        veloren_client.send_chat(format!("{}", msg.content));
                    }

                    for event in events {
                        match event {
                            VelorenEvent::Chat(text) => messages_tx.send(VelorenMessage::Chat(text)).unwrap(),
                        }
                    }
                    veloren_client.cleanup();

                    clock.tick(Duration::from_millis(1000 / TPS));
                }
            }
        });

        Self {
            messages_rx: Mutex::new(Some(messages_rx)),
            server_addr: addr.into(),
            guild,
            bridge_channel,
            discord_msgs_tx: Mutex::new(discord_msgs_tx),
        }
    }
}

impl EventHandler for Handler {
    fn ready(&self, ctx: Context, ready: Ready) {
        println!("Connected as {}", ready.user.name);

        ctx.set_presence(
            Some(Game {
                kind: GameType::Playing,
                name: "Veloren".to_string(),
                url: Some("https://www.veloren.net/".to_string()),
            }),
            OnlineStatus::Online,
        );

        let messages_rx = self.messages_rx.lock().unwrap().take().unwrap();
        let channel_id = ChannelId(self.bridge_channel);
        let server_addr = self.server_addr.clone();

      let handle = thread::spawn(move || loop {
            let msg = match messages_rx.try_recv() {
                Ok(msg) => msg,
                Err(mpsc::TryRecvError::Empty) => {
                    thread::sleep(Duration::from_millis(100));
                    continue;
                },
                _ => break,
            };

            let send_msg = |text| channel_id.send_message(|m|
                m.embed(|e|
                    e.colour(Colour::blurple()).title(text)));

            match msg {
                VelorenMessage::Chat(chat_msg) => if !chat_msg.starts_with("[D:") {
                    send_msg(chat_msg);
                },
                VelorenMessage::Connect => {
                    ctx.online();
                    //send_msg(format!("Server bridge to {} is connected.", server_addr));
                },
                VelorenMessage::Disconnect => {
                    ctx.invisible();
                    //send_msg(format!("Server bridge to {} is disconnected.", server_addr));
                },
            }
        });
    }

    fn resume(&self, _: Context, _: ResumedEvent) {
        println!("Resumed");
    }

    fn message(&self, _: Context, msg: Message) {
        let guild = GuildId(self.guild);

        if *msg.channel_id.as_u64() == self.bridge_channel {
            //println!("{:?}", msg);
            if !msg.author.bot {
                self.discord_msgs_tx.lock().unwrap().send(DiscordMessage::Chat {
                    nickname: msg.author.nick_in(guild).unwrap_or(msg.clone().author.name),
                    msg,
                }).unwrap();
            }
        }
    }
}

fn main() {
    kankyo::load()
        .expect("Could not find '.env' in working directory.");

    let token = env::var("DISCORD_TOKEN")
        .expect("No environment variable 'DISCORD_TOKEN' found.");
    let veloren_server = SocketAddr::from_str(&env::var("VELOREN_SERVER")
        .expect("No environment variable 'VELOREN_SERVER' found."))
        .expect("'VELOREN_SERVER' must be a valid server address with a port");
    let guild = env::var("DISCORD_GUILD")
        .expect("No environment variable 'DISCORD_GUILD' found.")
        .parse()
        .expect("'DISCORD_GUILD' must be a number");
    let bridge_channel = env::var("DISCORD_CHANNEL")
        .expect("No environment variable 'DISCORD_CHANNEL' found.")
        .parse()
        .expect("'DISCORD_CHANNEL' must be a number");

    let handler = Handler::new(veloren_server, guild, bridge_channel);

    let mut client = Client::new(&token, handler)
        .expect("Failed to create client.");

    client.start()
        .expect("Failed to start client.");
}
